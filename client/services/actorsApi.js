angular.module('moviesApp').service('ActorsApi', function($http) {
  this.getActorById = id => {
    return $http.get('https://api.themoviedb.org/3/person/' + id + '?api_key=c1d7ab2c955b1d0a24e3187c3a62efea&language=pl');
  }

  this.getMovieCredits = id => {
    return $http.get('https://api.themoviedb.org/3/person/' + id + '/movie_credits' + '?api_key=c1d7ab2c955b1d0a24e3187c3a62efea&language=pl');
  }

  this.getPopular = (page) => {
    return $http.get('https://api.themoviedb.org/3/person/popular' + '?api_key=c1d7ab2c955b1d0a24e3187c3a62efea&language=pl&page=' + page);
  }
});