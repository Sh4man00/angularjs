angular.module('moviesApp').service('MoviesApi', function($http) {
  this.getPopular = page => {
    return $http.get('https://api.themoviedb.org/3/discover/movie?api_key=c1d7ab2c955b1d0a24e3187c3a62efea&sort_by=popularity.desc&language=pl&page=' + page);
  }

  this.getPremieres = () => {
    return $http.get('https://api.themoviedb.org/3/discover/movie?api_key=c1d7ab2c955b1d0a24e3187c3a62efea&primary_release_date.gte=2018-05-01&primary_release_date.lte=2018-05-31&language=pl');
  }

  this.getMovieById = id => {
    return $http.get('https://api.themoviedb.org/3/movie/' + id + '?api_key=c1d7ab2c955b1d0a24e3187c3a62efea&language=pl&append_to_response=credits');
  }
});